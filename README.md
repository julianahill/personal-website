# juliana hill
a repo for my personal website ^.^

This repository is an old React project I created a few years ago that incorporates a collection of standardized algebraic puzzles, called `Project Euler`. I am refactoring my version of the project to display my understanding of older versions of React (a lot of features have been deprecated). My more recent projects are written using React 17 with hooks. 

## Coding hints

### BST
Notice how simple finding the second largest node got when we divided it into two cases:

1. The largest node has a left subtree.
2. The largest node does not have a left subtree

### Stacks
Two common uses for stacks are:

1. parsing (like in this problem)
1. tree or graph traversal (like depth-first traversal)
