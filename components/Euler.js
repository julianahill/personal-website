import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Layout } from 'antd'

import Nav from './Nav'
import Project from './euler/Project'

const Euler = () => (
  <Layout>
    <Nav/>
    <Project/>
  </Layout>
);

ReactDOM.render(<Euler/>, document.getElementById('euler'));