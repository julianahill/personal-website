import React, { Component } from 'react'
import { Layout, Menu } from 'antd'

const { Header } = Layout

import { env } from '../utils'

const Nav = () => {

  setTimeout(images, 300);

  const path = window.location.pathname.split('/').filter(p => p);
  const tab = path[0];

  let selected = '1';
  if (tab == 'euler' && path.length > 1) {
    selected = '';
  } else if (tab == 'license') {
    selected = '2';
  }

  return (
    <Header className="light">
      <Menu
        theme="light"
        mode="horizontal"
        defaultSelectedKeys={selected ? [selected] : []}
        style={{ lineHeight: '64px' }}
        selectedKeys={selected ? [selected] : []}
      >
        <a key="0" href='/'>
          <img
            height='50'
            src='/favicon.ico'
            style={{ backgroundColor: 'transparent' }}
          />
        </a>
        <Menu.Item key="1">
          <a href='/euler'>problems</a>
        </Menu.Item>
        {
          env !== 'prod' && <Menu.Item key="2">
            <a href='/license'>license</a>
          </Menu.Item>
        }
      </Menu>
    </Header>
  );

}

export default Nav
