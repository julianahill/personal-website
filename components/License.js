import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Layout } from 'antd'

import Nav from './Nav'
import Menu from './license/Menu'

const License = () => (
  <Layout>
    <Nav/>
    <Menu/>
  </Layout>
);

ReactDOM.render(<License/>, document.getElementById('license'));