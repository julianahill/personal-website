import React, { Component } from 'react'

import Submit from './Submit'

import { Progress, Layout } from 'antd'

const { Content } = Layout

import std from '../../utils/std'
import { grabInfo } from '../../utils/create'

export default class Single extends Component {

  state = {
    data: null,
    to_render: (
      <Content className='problem'>loading...</Content>
    ),
  };

  async componentDidMount () {

    const { id } = this.props;
    const parse = new DOMParser();

    const url = `${window.location.origin}/euler/${id}`;
    await fetch(url, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    }).then(res => res.json()).then(({ result }) => {

      const data = result;
      this.setState({ data });
    
      try {
        const html = parse.parseFromString(`<html><body>${data.problem}</body></html>`, 'text/html');
        this.setState({
          to_render: this.toReactComponents(html.body, data),
        });
      } catch (err) { 
        console.log(err);
      }
        
    }).catch(err => console.log(err));

  }

  grabHeaderContent = (data) => {

    const { color, percent } = data;
    
    const header = (<h1>Problem {data.id}</h1>);
    const difficulty = <Progress 
      percent={Number(percent)} 
      status='active'
      showInfo={false}
      strokeColor={color}
    />;

    return {
      header,
      difficulty,
    };

  }

  grabChildren = (elm, list) => {

    if (!elm.childNodes || !elm.childNodes.length) {
      return list;
    }

    for (let e of elm.childNodes) {

      if (e.nodeType >= 3 && e.textContent.trim()) {

        list.push(e.textContent);

      } else if (e.nodeName.toLowerCase() === 'img') {

        let origin = `${window.location.origin}/euler/`;
        let src = `https://projecteuler.net/${e.src.replace(origin, '')}`;

        list.push(<img key={std.string(6)} src={src} />);

      } else if (e.nodeType <= 2 && e.nodeName.toLowerCase() !== 'br') {
          
        const info = grabInfo(e);
        info[1].key = std.string(12);
        
        list.push(React.createElement(
          info[0],
          info[1],
          this.grabChildren(e, info[2])
        ));

      } else if (e.nodeName.toLowerCase() === 'br') {

        list.push(<br key={std.string(12)}/>);
          
      }
    } 
    
    return list;

  }

  toReactElement = (elm) => {

    const info = grabInfo(elm);
    info[1].key = std.string(12);
    
    return React.createElement(
      info[0], 
      info[1],
      this.grabChildren(elm, info[2]) 
    );
        
  }

  grabContent = elms => {

    const children = [];

    for (let c of elms) {
        
      if (c.nodeType <= 2 && c.nodeName.toLowerCase() !== 'br') {

        if (c.nodeName.toLowerCase().indexOf('img') > -1) {

          let origin = `${window.location.origin}/euler/`;
          let src = `https://projecteuler.net/${c.src.replace(origin, '')}`;

          children.push((<img key={std.string(6)} src={src} />));

        } else children.push(this.toReactElement(c));

      } else if (c.nodeType === 3 && c.textContent.trim()) {
          
        children.push(<p key={std.string(6)}>{c.textContent}</p>);

      } else if (c.nodeName.toLowerCase() === 'br') {

        children.push(<br key={std.string(12)}/>);

      }
    }

    return children;

  }

  toReactComponents = (html, data) => {

    let result;
    if (html && html.children.length) {
      let header, difficulty, content;

      const res = this.grabHeaderContent(data);
      header = res.header;
      difficulty = res.difficulty;

      for (let e of html.children) {
        content = (content || []).concat(
          this.grabContent(e.childNodes)
        );
      }

      result = (
        <Content className='problem'>
          <aside className='left'>
            {header}
            {!difficulty && !!content && <div style={{ minHeight: '10px', width: '100%' }} />}
            {difficulty}
            {!!difficulty && !!content && <div style={{ minHeight: '10px', width: '100%' }} />}
            {content}
          </aside>
          <aside className='right'>
            <Submit data={data.solution || ''}/>
          </aside>
        </Content>
      );         
    }

    return result;

  }

  render () {
    setTimeout(images, 500);
    const { to_render } = this.state;
    return to_render;
  }

}