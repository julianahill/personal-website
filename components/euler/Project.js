import React, { Component } from 'react'

import All from './All'
import Single from './Single'

import { Layout, Button } from 'antd'

const { Content } = Layout

import { MAX, LIMIT, TOTAL, COLORS } from '../../utils/constants'
const random = (max, start) => (start || 0) + Math.floor(Math.random() * max);

export default class Project extends Component {
  
  constructor (props) {
    super(props);
    this.state = {
      page: 0,
      data: [],
      loading: true,
    }; 
  }

  componentDidMount () {
      
    const pathing = window.location.pathname.split('/');
    let data = new Array(LIMIT).fill({});

    if (pathing.length > 2) {
      data = [{
        id: Number(pathing[pathing.length - 1])
      }];
    } else data = data.map(
      (d, i) => ({
        id: i + 1,
        color: COLORS[random(4)],
      })
    ).filter(d => d.id <= MAX);

    this.setState({
      data,
      loading: false,
    });

  }

  run = p => {

    let data = new Array(LIMIT).fill({});

    data = data.map(
      (d, i) => ({
        id: i + 1 + (p * LIMIT),
        color: COLORS[random(4)],
      })
    ).filter(d => d.id <= MAX);

    this.setState({
      data,
      page: p,
      loading: false,
    });

  }

  onNext = e => {
    const { page, loading } = this.state;
    if (page >= TOTAL || loading) return;
    this.setState({ loading: true });
    this.run(page + 1); 
  }

  onBack = e => {
    const { page, loading } = this.state;
    if (!page || loading) return;
    this.setState({ loading: true });
    this.run(page - 1);   
  }

  render () {

    const { data, loading } = this.state;
    let to_render = <Content className='problem'>loading...</Content>;
    
    if (!loading && data.length > 1)
      to_render = data.map(
        d => <All key={'problem-'+d.id} data={d}/>
      );
    else if (!loading && data.length)
      to_render = <Single data={data[0]} id={data[0].id} />;

    return loading || data.length < 2 ? to_render : (
      <Content className='problem'>
        <Button type='primary' shape='circle' size='large' onClick={this.onBack} icon='<' className='left'/>
        <Button type='primary' shape='circle' size='large' onClick={this.onNext} icon='>' className='right'/> 
        <ul className='problems'>{to_render}</ul>
      </Content>
    );

  }
}