import React, { Component } from 'react'

const All = ({ data }) => (
  <li className={data.color}>
    <a href={`/euler/${data.id}`}>Problem {data.id}</a>
  </li>
);

export default All