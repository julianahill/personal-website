import React, { Component } from 'react'

class Submit extends Component {

  constructor (props) {

    super(props);

    this.state = {
      output: [],
      data: props.data,
    };

  }

  componentDidMount () {

    const $this = this;

    console.log = function (message) {

      const { output } = $this.state;

      const tmp = output.map(m => m);
      tmp.push(message);

      $this.setState({
        output: tmp,
      });

    }

  }

  onText = ({ target }) => {

    const { value } = target;

    this.setState({
      data: value,  
    });

  }

  execute = e => {

    e.preventDefault();

    const { data } = this.state;

    Function(`${data}`)();

  }

  render () {

    const { data, output } = this.state;

    return <aside className='code'>
      
      <textarea
        value={data}
        className="input"
        onChange={this.onText}
      />
      
      {
        !!data && <ul className="output">{
          output.map(
            (text, i) => <li key={`output-${i}`}>{text}</li>
          )
        }</ul>
      }

      { !!data && <div className='button run' onClick={this.execute}>&gt;</div> }

    </aside>;
  }

}

export default Submit