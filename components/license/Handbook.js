import React, { Component } from 'react'
import { Layout } from 'antd'

const { Content } = Layout

import Question from './Question'

export default class Handbook extends Component {

  constructor (props) {
    const { id, page } = props;
    super(props);
    this.state = {
      id,
      sentences: [],
      text: page.text,
    };
  }

  async componentDidMount () {
    const { id } = this.state;
    await fetch(`/license/question/${id}`).then(
      res => res.json()
    ).then(
      ({ success, result }) => {
        if (!success) return alert(result.message);
        const { sentences } = result;
        this.setState({ sentences });
      }
    ).catch(() => {});
  }

  render () {
    const { text, sentences } = this.state;
    return (
      <Content className='problem'>
        <aside style={{ alignContent: 'start' }} className='left'>
          {
            text.split('\n').map(
              (t, i) => <p
                key={`text-${i}`}
                style={{ width: '100%' }}
              >{t}</p>
            )
          }
        </aside>
        <aside className='right'>
          {
            sentences.length ?
              <Question sentences={sentences}/> :
              <div>Generating question...</div>
          }
        </aside>
      </Content>
    );
  }

}
