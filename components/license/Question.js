import React, { Component } from 'react'

import { std, ask } from '../../utils'

export default class Question extends Component {

  constructor (props) {

    super(props);

    const { sentences } = props;

    const len = sentences.length;
    const index = std.random(len - 1, 1);
    
    const result = ask(sentences[index]);

    this.state = {
      ...result,
      selected: -1,
    };

  }

  onSelect = selected => this.setState({ selected });

  render () {
    
    const { answer, question, options, selected } = this.state;
    
    return <div style={{ width: '100%' }}>

      <h2 style={{
        margin: '0',
        padding: '20px'
      }}>{question}</h2>

      <ul>{
        options.map(
          (o, i) => <li
            key={`option-${i}`}
            style={{
              marginBottom: '20px',
              color: i == selected ? selected == answer ? 'green' : 'red' : 'black'
            }}
            onClick={() => this.onSelect(i)}
          >{o}</li>
        )
      }</ul>

    </div>;

  }

}
