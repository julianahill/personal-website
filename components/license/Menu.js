import React, { Component } from 'react'
import { Layout } from 'antd'

const { Content } = Layout

import Handbook from './Handbook'
import { pages } from '../../public/handbook'

const Menu = () => {

  const path = window.location.pathname.split('/').filter(p => p);
  
  const len = path.length;
  if (len > 1) return (
    <Handbook
      id={Number(path[len - 1])}
      page={
        pages[
          Number(path[len - 1])
        ]
      }
    />
  );

  else return (
    <Content className='problem'>
      <ul className='problems'>{
        pages.map(
          (p, i) => <li>
            <a href={`/license/${i}`}>Section {i + 1}</a>
          </li>
        )
      }</ul>
    </Content>
  );
};

export default Menu