function response (err, data) {
  if (err) {
    console.log(err);
    return {
      success: false,
      result: err
    };
  } return {
    success: true,
    result: data
  };
}

function random (max, start) {
  return (start || 0) + Math.floor(Math.random() * max);
}

function string (length) {
  let i = 0,
      str = '',
      p = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
          'abcdefghijklmnopqrstuvwxyz' +
          '0123456789';

  for (; i < length; i++,
     str += p[random(p.length)]
   );

  return str;
}

function shuffle (array) {
  
  const tmp = [];
  const len = array.length;
  
  for (let i = 0; i < len; i++) {
    
    tmp.push(String(array[i]));
    
    let index = random(tmp.length - 1);
    if (index != i) {
      tmp[i] = String(tmp[index]);
      tmp[index] = String(array[i]);
    }

  }

  return tmp;

}

function cleanUpData (data) {
  for (let key in data) {
    if (!data[key]) continue;
    data[key] = data[key].toString().trim();
  } return data;
}

function grabURL (req) {
  const host = req.headers.host;
  const pre = host.indexOf('localhost') !== -1 ? 'http://' : 'https://';
  return { host, pre };
}

module.exports = {
  string,
  random,
  grabURL,
  shuffle,
  response,
  cleanUpData,
}