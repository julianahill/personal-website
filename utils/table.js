const path = require('path');
const keyFilename = path.join(__dirname, '/gcp.json');

const Table = require('@thefinerthings/table');
const table = Table({
  gcp: {
    keyFilename,
    projectId: 'julianahill',
  }
});

module.exports = table;
