export const MAX = 790;
export const LIMIT = 50;
export const TOTAL = Math.floor(MAX / LIMIT);
export const COLORS = ['green', 'yellow', 'orange', 'red'];