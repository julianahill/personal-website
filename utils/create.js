function grabInfo (elm) {
    const info = [null, {}, []];
    info[0] = elm.nodeName.toLowerCase();

    if (elm.attributes.id) {
        info[1].id = elm.getAttribute('id');
    } if (elm.classList) {
        info[1].className = '';
        for (let i of elm.classList) {
            info[1].className += i + ' ';
        } if (!info[1].className) {
            delete info[1].className;
        }
    } if (elm.attributes.href) {
        info[1].target='_blank';
        info[1].href = `https://projecteuler.net/${elm.getAttribute('href')}`;
    } if (elm.attributes.src) {
        info[1].src = `https://projecteuler.net/${elm.getAttribute('src')}`;
    }

    return info;
}

module.exports = { grabInfo }