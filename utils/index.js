const std = require('./std');

const env = false ? 'local' : 'prod';
const ask = sentence => {

  let answer = -1, options = [], question = '';
  if (!sentence || !sentence.length) return {
    answer,
    options,
    question,
  };
  
  const to_select = sentence.filter(s => s.split(' ').length > 2);

  const len = to_select.length;
  const index = std.random(len - 1);

  let process = to_select[index];
  if (process.indexOf('•') > -1) {
    process = process.split('•');
  } else if (process.indexOf(' by ') > -1) {
    process = process.split(' by ');
    process[0] += ' by:';
  } else if (process.indexOf(' that ') > -1) {
    process = process.split(' that ');
    process[0] += ' that:';
  } else if (process.indexOf(' is ') > -1) {
    process = process.split(' is ');
    process[0] += ' is:';
  } else if (process.indexOf(' are ') > -1) {
    process = process.split(' are ');
    process[0] += ' are:';
  } else if (process.indexOf(' on ') > -1) {
    process = process.split(' on ');
    process[0] += ' on:';
  } else if (process.indexOf(' to ') > -1) {
    process = process.split(' to ');
    process[0] += ' to:';
  } else if (process.indexOf(' at ') > -1) {
    process = process.split(' at ');
    process[0] += ' at:';
  } else if (process.indexOf(' follow') > -1) {
    process = process.split(' follow');
    process[0] += ' follow:';
  } else if (process.indexOf(' about ') > -1) {
    process = process.split(' about ');
    process[0] += ' about:';
  } else if (process.indexOf(', ') > -1) {
    process = process.split(', ');
    process[0] += ':';
  }

  process = process.map(p => p.trim());

  const procs = process.length;
  if (procs >= 3) {

    question = process[0];
    for (let i = 1; i < 3; i++) {
      options.push(process[i]);
    } 
    
    options.push('Both');
    options = std.shuffle(options);

    answer = options.indexOf('Both');

  } else {

    question = process[0];
    options.push(process[1]);

    const words = process[1].split(' ');
    const gen_word = max => words[std.random(max || words.length - 1)];

    const word = words.length > 1 ? gen_word() : words[0];
    const w_i = words.indexOf(word);

    options.push(
      process[1].replace(word, w_i ? gen_word(w_i) : std.string(word.length))
    );

    options.push(
      process[1].replace(word, w_i ? gen_word(w_i) : std.string(word.length))
    );

    options = std.shuffle(options);
    answer = options.indexOf(process[1]);

  }

  return {
    answer,
    options,
    question,
  };

};

module.exports = { std, env, ask };