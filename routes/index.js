const express = require('express');
const router = express.Router();

const std = require('../utils/std');

const title = 'juliana hill';
const description = `
  This Ana\'s personal website. 
  Pizza lover, and of component-based web design.
  She graduated from Purdue University with a degree 
  in Computer Science, and is currently a Software Engineer.
`;

const keywords = 'Top 10, Top Ten, Juliana Amelia Hill, Mia Hill, Juliana Hill, Generation Z, Millenials, Gen Z, Zoomers, Gen Y';
const url = 'https://www.nationalgeographic.com/photography/photo-of-the-day/_jcr_content/.syndication-gallery.json';

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('static', {
    url, title, description, ...std.grabURL(req), keywords
  });
});

/* GET home page. */
router.get('/app', (req, res, next) => {
  res.render('index', {
    title, description, ...std.grabURL(req)
  });
});

module.exports = router;
