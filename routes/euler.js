const fetch = require('node-fetch');

const express = require('express');
const router = express.Router();

const std = require('../utils/std');
const euler = require('../controllers/euler');

const title = 'project euler | juliana hill';
const description = 'Ana\'s Project Euler. Over 700 puzzles to solve.';

router.get('/', (req, res, next) => {
  res.render('euler', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/count', async (req, res, next) => {
  res.json(
    std.response(null, { count: 790 })
  );
});

router.get('/:id', (req, res, next) => {
  res.render('euler', {
    title, description, ...std.grabURL(req)
  });
});

router.post('/:id', async (req, res, next) => {

  const { id } = req.params;

  const url = `https://projecteuler.net/problem=${id}`;
  const min = `https://projecteuler.net/minimal=${id}`;

  const puzzle = await euler.get(id).catch(() => null);
  const percent = puzzle && puzzle.percent ? puzzle.percent : await fetch(url).then(
    resp => resp.text()
  ).then(
    html => html.toLowerCase()
  ).then(html => {
    const match = 'difficulty rating:';

    const mi = html.indexOf(match);
    let tmp = html.substring(mi);

    const pi = tmp.indexOf('%');
    tmp = tmp.substring(match.length, pi);

    return Number(tmp);
  }).catch(() => 0);

  let color = puzzle && puzzle.color ? puzzle.color : 'green';
  if (!puzzle && percent > 30) color = 'yellow';
  if (!puzzle && percent > 50) color = 'orange';
  if (!puzzle && percent > 70) color = 'red';

  const problem = !puzzle || !puzzle.problem ?
    await fetch(min).then(resp => resp.text()).catch(() => '') :
    puzzle.problem;

  if (!puzzle)
    await euler.create({
      id,
      color,
      percent,
      problem,
    }).catch(err => console.log(err));
  else if (puzzle && !puzzle.problem)
    await euler.update(id, { problem }).catch(
      err => console.log(err)
    );

  res.json(
    std.response(null, {
      id,
      color,
      percent,
      problem,
      solution: puzzle ? puzzle.solution : '',
    })
  );

});

module.exports = router;
