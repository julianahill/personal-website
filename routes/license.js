const express = require('express');
const router = express.Router();

const std = require('../utils/std');
const { pages } = require('../public/handbook');

const title = 'license | juliana hill';
const description = 'Ana\'s CA DL Exam Practice.';

router.get('/', (req, res, next) => {
  res.render('license', {
    title, description, ...std.grabURL(req)
  });
});

router.get('/:id', (req, res, next) => {

  const id = Number(req.params.id);
  if (Number.isNaN(id)) 
    return res.json(
      std.response({
        messages: `Section, ${req.params.id}, does not exist!`
      })
    );
  
  res.render('license', {
    description,
    title: `Section ${id + 1} | License`,
    ...std.grabURL(req),
  });

});

router.get('/question/:id', async (req, res, next) => {

  const id = Number(req.params.id);
  if (Number.isNaN(id)) 
    return res.json(
      std.response({
        messages: `Section, ${req.params.id}, does not exist!`
      })
    );

  const { text } = pages[id];
  const sentences = text.split('\n').map(s => s.trim()).reduce(
    (a, c) => {

      let tmp = a.map(t => t);

      if (
        tmp.length && (
          c.length || c.indexOf('•') > -1
        )
      ) tmp[tmp.length - 1] += c + ' ';
      else tmp.push(c);

      return tmp;

    }, []
  ).map(
    sentence => sentence.split('.').map(
      s => s.trim()
    ).filter(s => s)
  ).filter(s => s.length);
  
  res.json(
    std.response(null, { sentences })
  );

});

module.exports = router;
