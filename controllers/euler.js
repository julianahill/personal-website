const table = require('../utils/table');
const name = table.set('euler', 'id').TableName;

module.exports = {
  
  table: () => table.table(name),

  get: (id) => table.get(name, id),
  all: (last, limit) => table.all(name, last, limit),

  grab: (params, last, limit) => table.grab(name, params, last, limit),
  find: (params, last, limit) => table.find(name, params, last, limit),
  index: (index, params, last, limit) => table.index(name, index, params, last, limit),
  search: (index, key, values, limit) => table.index_search(name, index, key, values, limit),

  create: (params) => table.get(name, params.id).then(data => {
    if (!data) return table.create(name, params).then(() => params);
    else return data;
  }),

  update: (id, params) => table.get(name, id).then(data => {
    if (data) return table.update(name, id, params).then(
      () => ({ ...params, id })
    );
    else return { message: 'Item does not exist!' };
  }),

  delete: (id) => table.get(name, id).then(data => {
    if (data) return table.delete(name, id);
  }),

}