const state = {};
const app = document.getElementById('app');
const url = 'https://www.nationalgeographic.com/photography/photo-of-the-day/_jcr_content/.syndication-gallery.json';

const read = (url, callback) => {
  fetch('/images').then(res => res.json())
    .then(res => callback(res))
    .catch(err => callback({
      success: false,
      result: err
    }))
};

/* HTML Templates */
const html = () => `
  <img hidden id='img' class='background' />
  <header class="header">
    <aside class="circle outer">
      <ul class="circle inner" onclick="window.location = '/euler'">
        <li class="title">Juli[ana]<br>Hill<span class="blinking">|</span></li>
        <li class="sub-title">BS | SWE | CS</li>
      </ul>
    </aside>
  </header>
`;

/* Custom data fetching */
const background_images = () => {
  read(url, ({ items }) => {
    if (!items) return;
    const uris = items.map(i => i.image && i.image.uri).filter(i => i);
    state.images = uris;
    animate();
  });
};

const animate = () => {
  const img = document.getElementById('img');
  
  let index = 0;
  const rotate = () => {
    img.style = 'opacity: 0;';
    setTimeout(() => img.setAttribute('src', state.images[index]), 650);
    setTimeout(() => img.style =  'opacity: 1;', 2000);
    if (index < state.images.length - 1) index++;
    else index = 0;
  }

  setTimeout(rotate, 300);
  setInterval(rotate, 7000);

  img.removeAttribute('hidden');
};

app.innerHTML = html();
setTimeout(() => background_images(), 200);